import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { globalStats, globalStatsModelByAPI } from '../models/covidStatsModel'

@Injectable({
  providedIn: 'root',
})

export class covidStatsService{

        private covidStats: globalStats={
            cases:0,
            deaths:0,
            recovered:0
        }

    constructor(private http: HttpClient){

        }

    

    public fetchGlobalStats():void{

        this.http.get<globalStatsModelByAPI>(`https://covid19.mathdro.id/api`)
        .subscribe(stats=>{
            this.covidStats.cases=stats.confirmed.value
            this.covidStats.deaths=stats.deaths.value
            this.covidStats.recovered=stats.recovered.value
            console.log("model is:",this.covidStats);
            
        });

    }
    public getGlobalCovidStats(){
        this.fetchGlobalStats
        return this.covidStats
    }



}



