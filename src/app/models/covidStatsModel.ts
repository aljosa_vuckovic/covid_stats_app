export interface globalStats{
    cases:number;
    deaths:number;
    recovered:number;
}

export interface globalStatsModelByAPI{
    confirmed: {
        value : number,
        detail: string
    },
    recovered: {
        value : number,
        detail: string
    },
    deaths: {     
        value : number,
        detail: string
    },
}