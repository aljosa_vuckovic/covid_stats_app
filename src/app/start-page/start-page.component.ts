import { Component, OnInit } from '@angular/core';
import { covidStatsService } from '../services/covidStatsServices';
import { globalStats } from '../models/covidStatsModel';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {



  constructor(private covidStatsService: covidStatsService) { }

  ngOnInit(): void {
    this.covidStatsService.fetchGlobalStats()
  }

  get globalStats(){

    console.log("le here",this.covidStatsService.getGlobalCovidStats());
    
    return this.covidStatsService.getGlobalCovidStats()
    
  }
  

}
